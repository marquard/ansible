FROM alpine:latest

# Install the latest updates.
RUN apk update -Uv --no-progress

# Install Python.
RUN \
  apk add -uv --no-progress \
    python3 \
    python3-dev && \
  rm -rf /var/cache/apk/*

# Install pip and upgrade it.
RUN \
  apk add -uv --no-progress py3-pip && \
  rm -rf /var/cache/apk/* && \
  pip install --upgrade pip

# Install Ansible.
RUN \
  apk add -uv --no-progress ansible && \
  rm -rf /var/cache/apk/*

# Install ansible-lint.
RUN \
  apk add --virtual=build -uv --no-progress --no-cache \
    gcc \
    libffi-dev \
    make \
    musl-dev \
    openssl-dev \
    python2-dev && \
  pip --no-cache-dir install ansible-lint && \
  apk del --purge build
